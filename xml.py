# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup as BS
import urllib.request

url = "https://ad-api-v01-jp-dev.ulizaex.com/reqVAST.php?adcontrol=1&condition_id=6416&v=2&page-url=%5Bpage-url%5D&episode_code=%5Bepisode_code%5D&uuid=%5Buuid%5D&random=%5Brandom%5D&at=1"
xml = urllib.request.urlopen(url).read()
soup = BS(xml, 'xml')

AdSystemtag = soup.find("AdSystem").text
print(AdSystemtag)
